package design.patterns.adapter;

public interface Adapter {
    double getSpeed();
}
