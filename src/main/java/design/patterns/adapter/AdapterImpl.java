package design.patterns.adapter;

public class AdapterImpl implements Adapter{
    private Movable ferrari;

    public AdapterImpl(Movable ferrari) {
        this.ferrari = ferrari;
    }

    @Override
    public double getSpeed() {
        return converter(ferrari.getSpeed());
    }
     public double converter(double speed){
        return speed*2;
     }
}
