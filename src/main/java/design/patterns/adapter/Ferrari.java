package design.patterns.adapter;

public class Ferrari implements Movable {
    @Override
    public double getSpeed() {
        return 268;
    }
}
