package design.patterns.adapter;

public class Application {

    public static void main(String[] args) {


		Movable bugattiVeyron = new Ferrari();
		Adapter bugattiVeyronAdapter = new AdapterImpl(bugattiVeyron);

		System.out.println(bugattiVeyronAdapter.getSpeed());



	}
}

