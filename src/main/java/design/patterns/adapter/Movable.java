package design.patterns.adapter;

public interface Movable {
    // returns speed in MPH
    double getSpeed();
}
