package design.patterns.decorator;

public interface Car {

    public void assemble();
}
