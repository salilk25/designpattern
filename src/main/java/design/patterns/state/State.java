package design.patterns.state;

public interface State {

    void onEnterState();

    void observe();
}

class PeacefulState implements State {

    private final Mammoth mammoth;

    public PeacefulState(Mammoth mammoth) {
        this.mammoth = mammoth;
    }

    @Override
    public void observe() {
   System.out.println("{} is calm and peaceful.");
    }

    @Override
    public void onEnterState() {
       System.out.println("{} calms down.");
    }
}

class AngryState implements State {

    private final Mammoth mammoth;

    public AngryState(Mammoth mammoth) {
        this.mammoth = mammoth;
    }

    @Override
    public void observe() {
     System.out.println("{} is furious!");
    }

    @Override
    public void onEnterState() {
        System.out.println("{} gets angry!");
    }
}
