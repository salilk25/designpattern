package design.patterns.flyweight;

public interface Employee {
    public void assignSkill(String skill);
    public void task();
}
