package design.patterns.bridge;

public interface Color {
    void applyColor();
}
