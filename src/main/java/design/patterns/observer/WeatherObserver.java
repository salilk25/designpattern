package design.patterns.observer;

public interface WeatherObserver {

    void update(WeatherType currentWeather);
}


class Orcs implements WeatherObserver {

    @Override
    public void update(WeatherType currentWeather) {
        System.out.println("The orcs are facing " + currentWeather.getDescription() + " weather now");
    }
}


class Hobbits implements WeatherObserver {

    @Override
    public void update(WeatherType currentWeather) {
            System.out.println("The hobbits are facing " + currentWeather.getDescription() + " weather now");

    }
}
